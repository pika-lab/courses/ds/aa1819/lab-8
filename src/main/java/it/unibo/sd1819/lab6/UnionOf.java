package it.unibo.sd1819.lab6;

import java.util.Optional;
import java.util.function.Consumer;

public abstract class UnionOf<A, B> {

    public static <X, B> UnionOf<X, B> left(final X value) {
        return new Left<X, B>(value);
    }

    public static <X, Y> UnionOf<X, Y> right(final Y value) {
        return new Right<X, Y>(value);
    }

    private UnionOf() {

    }

    public boolean isLeft() {
        return asLeft().isPresent();
    }

    public boolean isRight() {
        return asRight().isPresent();
    }

    public abstract Optional<A> asLeft();

    public abstract Optional<B> asRight();

    public UnionOf<A, B> ifLeft(final Consumer<? super A> f) {
        asLeft().ifPresent(f);
        return this;
    }

    public UnionOf<A, B> ifRight(final Consumer<? super B> f) {
        asRight().ifPresent(f);
        return this;
    }

    public Object get() {
        if (isLeft()) {
            return asLeft().get();
        } else {
            return asRight().get();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Boolean.hashCode(isLeft());
        result = prime * result + Boolean.hashCode(isRight());
        result = prime * result + get().hashCode();
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final UnionOf<?, ?> other = (UnionOf<?, ?>) obj;
        return isLeft() == other.isLeft() && get().equals(other.get());
    }

    @Override
    public String toString() {
        return String.format("UnionOf.%s[%s]", getClass().getSimpleName(), get());
    }

    private final static class Left<A, B> extends UnionOf<A, B> {
        private final Optional<A> value;

        public Left(final A value) {
            this.value = Optional.of(value);
        }

        @Override
        public Optional<A> asLeft() {
            return value;
        }

        @Override
        public Optional<B> asRight() {
            return Optional.empty();
        }
    }

    private final static class Right<A, B> extends UnionOf<A, B> {
        private final Optional<B> value;

        public Right(final B value) {
            this.value = Optional.of(value);
        }

        @Override
        public Optional<A> asLeft() {
            return Optional.empty();
        }

        @Override
        public Optional<B> asRight() {
            return value;
        }
    }

}
