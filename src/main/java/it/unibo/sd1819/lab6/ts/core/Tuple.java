package it.unibo.sd1819.lab6.ts.core;

public interface Tuple {
    default boolean matches(final Template template) {
        return template.matches(this);
    }
}
