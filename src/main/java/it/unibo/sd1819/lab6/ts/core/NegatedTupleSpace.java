package it.unibo.sd1819.lab6.ts.core;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import it.unibo.sd1819.lab6.UnionOf;

public interface NegatedTupleSpace<T extends Tuple, TT extends Template> extends TupleSpace<T, TT> {
    CompletableFuture<TT> absent(TT template);

    CompletableFuture<UnionOf<TT, T>> tryAbsent(TT template);
}
