package it.unibo.sd1819.lab6.ts.objects;

import it.unibo.sd1819.lab6.ts.core.Tuple;
import org.omg.CORBA.Object;

import java.util.Objects;

public class ObjectTuple<T> implements Tuple {
    private final T object;

    private ObjectTuple(T object) {
        this.object = object;
    }

    public T get() {
        return object;
    }


    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectTuple<?> that = (ObjectTuple<?>) o;
        return Objects.equals(object, that.object);
    }

    @Override
    public int hashCode() {
        return Objects.hash(object);
    }

    @Override
    public String toString() {
        return "ObjectTuple{" + object + '}';
    }

    public static <X> ObjectTuple<X> of(X object) {
        return new ObjectTuple<>(Objects.requireNonNull(object));
    }
}
