package it.unibo.sd1819.lab6.ts;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import it.unibo.sd1819.lab6.UnionOf;
import org.apache.commons.collections4.MultiSet;

import it.unibo.sd1819.lab6.ts.core.ExtendedTupleSpace;
import it.unibo.sd1819.lab6.ts.core.Template;
import it.unibo.sd1819.lab6.ts.core.Tuple;
import org.apache.commons.collections4.multiset.HashMultiSet;

public abstract class AbstractTupleSpace<T extends Tuple, TT extends Template> implements ExtendedTupleSpace<T, TT> {

    private static final boolean DEBUG = false;

    private static ExecutorService globalExecutor;
    public static  ExecutorService globalExecutor() {
        if (globalExecutor == null) {
            globalExecutor = Executors.newSingleThreadExecutor();
        }
        return globalExecutor;
    }

    private final ExecutorService executor;
    private final ReentrantLock lock = new ReentrantLock(true);
    private final MultiSet<PendingRequest> pendingRequests = new HashMultiSet<>();
    private final String name;

    public AbstractTupleSpace(final String name, final ExecutorService executor) {
        this.name = Objects.requireNonNull(name) + "#" + System.identityHashCode(this);
        this.executor = Objects.requireNonNull(executor);
    }

    protected ReentrantLock getLock() {
        return lock;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    protected void log(final String format, final Object... args) {
        if (DEBUG) {
            System.out.println(String.format("[" + getName() + "] " + format + "\n", args));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractTupleSpace<?, ?> that = (AbstractTupleSpace<?, ?>) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    protected Collection<PendingRequest> getPendingRequests() {
        return pendingRequests;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public CompletableFuture<T> read(final TT template) {
        log("Requested `read` operation on template: %s", template);
        final CompletableFuture<T> result = new CompletableFuture<>();
        executor.execute(() -> this.handleRead(template, result));
        return result;
    }

    protected void handleRead(final TT template, final CompletableFuture<T> promise) {
        getLock().lock();
        try {
            final Optional<T> read = lookForTuple(template);
            if (read.isPresent()) {
                promise.complete(read.get());
                onRead(read.get());
            } else {
                pendingRequests.add(newPendingAccessRequest(RequestTypes.READ, template, promise));
            }
        } finally {
            getLock().unlock();
        }
    }

    protected Collection<T> lookForTuples(final TT template) {
        return lookForTuples(template, countTuples());
    }

    protected abstract Collection<T> lookForTuples(final TT template, int limit);

    protected abstract Optional<T> lookForTuple(final TT template);

    @Override
    public CompletableFuture<T> take(final TT template) {
        log("Requested `take` operation on template: %s", template);
        final CompletableFuture<T> result = new CompletableFuture<>();
        executor.execute(() -> this.handleTake(template, result));
        return result;
    }

    protected void handleTake(final TT template, final CompletableFuture<T> promise) {
        getLock().lock();
        try {
            final Optional<T> take = retrieveTuple(template);
            if (take.isPresent()) {
                promise.complete(take.get());
                onTaken(take.get());
            } else {
                final PendingRequest pendingRequest = newPendingAccessRequest(RequestTypes.TAKE, template, promise);
                pendingRequests.add(pendingRequest);
            }
        } finally {
            getLock().unlock();
        }
    }

    protected void onTaken(T tuple) {
        resumePendingAbsentRequests(tuple);
    }

    protected void onRead(T t) {}

    protected void onWritten(T t) {}

    protected Collection<T> retrieveTuples(TT template) {
        return retrieveTuples(template, countTuples());
    }

    protected abstract Collection<T> retrieveTuples(TT template, int limit);

    protected abstract Optional<T> retrieveTuple(TT template);

    @Override
    public CompletableFuture<T> write(final T tuple) {
        log("Requested `write` operation for of: %s", tuple);
        final CompletableFuture<T> result = new CompletableFuture<>();
        executor.execute(() -> this.handleWrite(tuple, result));
        return result;
    }

    protected void handleWrite(final T tuple, final CompletableFuture<T> promise) {
        getLock().lock();
        try {
            resumePendingAccessRequests(tuple).ifPresent(this::insertTuple);
            promise.complete(tuple);
            onWritten(tuple);
        } finally {
            getLock().unlock();
        }
    }

    protected abstract void insertTuple(T tuple);

    protected Optional<T> resumePendingAccessRequests(final T insertedTuple) {
        Optional<T> result = Optional.of(insertedTuple);
        final Iterator<PendingRequest> i = pendingRequests.iterator();
        while (i.hasNext()) {
            final PendingRequest pendingRequest = i.next();
            if (pendingRequest.getRequestType() != RequestTypes.ABSENT
                    && pendingRequest.getTemplate().matches(insertedTuple)) {
                i.remove();
                pendingRequest.getPromiseTuple().complete(insertedTuple);
                if (pendingRequest.getRequestType() == RequestTypes.TAKE) {
                    result = Optional.empty();
                    onTaken(insertedTuple);
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public CompletableFuture<Collection<? extends T>> get() {
        log("Requested `get` operation");
        final CompletableFuture<Collection<? extends T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleGet(result));
        return result;
    }

    protected void handleGet(final CompletableFuture<Collection<? extends T>> promise) {
        getLock().lock();
        try {
            final Collection<T> result = getAllTuples();
            promise.complete(result);
        } finally {
            getLock().unlock();
        }
    }

    protected abstract Collection<T> getAllTuples();

    @Override
    public CompletableFuture<Integer> getSize() {
        log("Requested `getSize` operation");
        final CompletableFuture<Integer> result = new CompletableFuture<>();
        executor.execute(() -> this.handleGetSize(result));
        return result;
    }

    protected abstract int countTuples();

    protected void handleGetSize(final CompletableFuture<Integer> promise) {
        getLock().lock();
        try {
            int count = countTuples();
            promise.complete(count);
        } finally {
            getLock().unlock();
        }
    }

    @Override
    public CompletableFuture<Collection<? extends T>> readAll(final TT template) {
        log("Requested `readAll` operation on template %s", template);
        final CompletableFuture<Collection<? extends T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleReadAll(template, result));
        return result;
    }

    protected void handleReadAll(final TT template, final CompletableFuture<Collection<? extends T>> promise) {
        getLock().lock();
        try {
            final Collection<T> result = lookForTuples(template);
            promise.complete(result);
            result.forEach(this::onRead);
        } finally {
            getLock().unlock();
        }
    }

    @Override
    public CompletableFuture<Collection<? extends T>> takeAll(final TT template) {
        log("Requested `takeAll` operation on template %s", template);
        final CompletableFuture<Collection<? extends T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleTakeAll(template, result));
        return result;
    }

    protected void handleTakeAll(final TT template, final CompletableFuture<Collection<? extends T>> promise) {
        getLock().lock();
        try {
            final Collection<T> result = retrieveTuples(template);
            promise.complete(result);
            result.forEach(this::onTaken);
        } finally {
            getLock().unlock();
        }
    }

    @Override
    public CompletableFuture<Collection<? extends T>> writeAll(final Collection<? extends T> tuples) {
        log("Requested `writeAll` operation on tuples: %s", tuples);
        final CompletableFuture<Collection<? extends T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleWriteAll(tuples, result));
        return result;
    }

    protected void handleWriteAll(final Collection<? extends T> tuples, final CompletableFuture<Collection<? extends T>> promise) {
        getLock().lock();
        final Collection<T> result = new LinkedList<>();
        try {
            for (final T tuple : tuples) {
                result.add(tuple);
                resumePendingAccessRequests(tuple).ifPresent(this::insertTuple);
            }
            promise.complete(result);
            result.forEach(this::onWritten);
        } finally {
            getLock().unlock();
        }
    }

    @Override
    public CompletableFuture<Optional<T>> tryTake(final TT template) {
        log("Requested `tryTake` operation on template: %s", template);
        final CompletableFuture<Optional<T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleTryTake(template, result));
        return result;
    }

    protected void handleTryTake(final TT template, final CompletableFuture<Optional<T>> promise) {
        getLock().lock();
        try {
            final Optional<T> take = retrieveTuple(template);
            promise.complete(take);
            take.ifPresent(this::onTaken);
        } finally {
            getLock().unlock();
        }
    }

    @Override
    public CompletableFuture<Optional<T>> tryRead(final TT template) {
        log("Requested `tryRead` operation on template: %s", template);
        final CompletableFuture<Optional<T>> result = new CompletableFuture<>();
        executor.execute(() -> this.handleTryRead(template, result));
        return result;
    }

    protected void handleTryRead(final TT template, final CompletableFuture<Optional<T>> promise) {
        getLock().lock();
        try {
            final Optional<T> take = lookForTuple(template);
            promise.complete(take);
            take.ifPresent(this::onRead);
        } finally {
            getLock().unlock();
        }
    }

    @Override
    public String toString() {
        return getClass().getName() + "{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public CompletableFuture<TT> absent(final TT template) {
        log("Requested `absent` operation for: %s", template);
        final CompletableFuture<TT> promise = new CompletableFuture<>();
        getExecutor().execute(() -> this.handleAbsent(template, promise));
        return promise;
    }

    private void handleAbsent(final TT template, final CompletableFuture<TT> promise) {
        getLock().lock();
        try {
            final Optional<T> read = lookForTuple(template);
            if (read.isPresent()) {
                getPendingRequests().add(newPendingAbsentRequest(template, promise));
            } else {
                promise.complete(template);
            }
        } finally {
            getLock().unlock();
        }
    }

    protected void resumePendingAbsentRequests(final T removedTuple) {
        final Iterator<PendingRequest> i = getPendingRequests().iterator();
        while (i.hasNext()) {
            final PendingRequest pendingRequest = i.next();
            if (pendingRequest.getRequestType() == RequestTypes.ABSENT
                    && pendingRequest.getTemplate().matches(removedTuple)
                    && !lookForTuple(pendingRequest.getTemplate()).isPresent()) {
                i.remove();
                pendingRequest.getPromiseTemplate().complete(pendingRequest.getTemplate());
            }
        }
    }

    @Override
    public CompletableFuture<UnionOf<TT, T>> tryAbsent(final TT template) {
        log("Requested `tryAbsent` operation for: %s", template);
        final CompletableFuture<UnionOf<TT, T>> promise = new CompletableFuture<>();
        getExecutor().execute(() -> this.handleTryAbsent(template, promise));
        return promise;
    }

    private void handleTryAbsent(final TT template,
                                 final CompletableFuture<UnionOf<TT, T>> promise) {
        getLock().lock();
        try {
            final Optional<T> read = lookForTuple(template);
            if (read.isPresent()) {
                promise.complete(UnionOf.right(read.get()));
            } else {
                promise.complete(UnionOf.left(template));
            }
        } finally {
            getLock().unlock();
        }
    }

    protected enum RequestTypes {
        READ, TAKE, ABSENT;
    }

    protected PendingRequest newPendingAccessRequest(final RequestTypes requestType, final TT template, final CompletableFuture<T> promiseTuple) {
        return new PendingRequest(requestType, template, promiseTuple, null);
    }

    protected PendingRequest newPendingAbsentRequest(final TT template, final CompletableFuture<TT> promiseTemplate) {
        return new PendingRequest(RequestTypes.ABSENT, template, null, promiseTemplate);
    }

    protected class PendingRequest {
        private final RequestTypes requestType;
        private final TT template;
        private final CompletableFuture<T> promiseTuple;
        private final CompletableFuture<TT> promiseTemplate;

        private PendingRequest(final RequestTypes requestType, final TT template, final CompletableFuture<T> promiseTuple, CompletableFuture<TT> promiseTemplate) {
            this.requestType = Objects.requireNonNull(requestType);
            this.template = Objects.requireNonNull(template);
            this.promiseTuple = promiseTuple;
            this.promiseTemplate = promiseTemplate;
        }

        public RequestTypes getRequestType() {
            return requestType;
        }

        public TT getTemplate() {
            return template;
        }

        public CompletableFuture<T> getPromiseTuple() {
            return promiseTuple;
        }

        public CompletableFuture<TT> getPromiseTemplate() {
            return promiseTemplate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PendingRequest that = (PendingRequest) o;
            return requestType == that.requestType &&
                    Objects.equals(template, that.template) &&
                    Objects.equals(promiseTuple, that.promiseTuple) &&
                    Objects.equals(promiseTemplate, that.promiseTemplate);
        }

        @Override
        public int hashCode() {
            return Objects.hash(requestType, template, promiseTuple, promiseTemplate);
        }

        @Override
        public String toString() {
            return "PendingRequest{" +
                    "requestType=" + requestType +
                    ", template=" + template +
                    ", promiseTuple=" + promiseTuple +
                    ", promiseTemplate=" + promiseTemplate +
                    '}';
        }


    }
}
