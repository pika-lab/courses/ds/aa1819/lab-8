package it.unibo.sd1819.lab6.ts.objects;

import it.unibo.sd1819.lab6.ts.core.Template;
import it.unibo.sd1819.lab6.ts.core.Tuple;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;

public class ObjectTemplate<T> implements Template, Predicate<Tuple> {

    private final Class<T> type;
    private final Predicate<T> predicate;

    private ObjectTemplate(Class<T> type, Predicate<T> predicate) {
        this.type = type;
        this.predicate = x -> predicate.test((T) x);
    }

    @Override
    public boolean matches(Tuple tuple) {
        if (tuple instanceof ObjectTuple) {
            final ObjectTuple<?> objectTuple = (ObjectTuple<?>) tuple;
            if (type.isAssignableFrom(objectTuple.get().getClass())) {
                final ObjectTuple<T> objectTupleOfT = (ObjectTuple<T>) tuple;
                return predicate.test(objectTupleOfT.get());
            }
        }
        return false;
    }

    public ObjectTemplate<T> where(Predicate<T> predicate) {
        return new ObjectTemplate<>(type, this.predicate.and(predicate));
    }

    public static <X> ObjectTemplate<X> anyOfType(Class<X> type, Predicate<X> predicate) {
        return new ObjectTemplate<>(type, predicate);
    }

    public static <X> ObjectTemplate<X> anyOfType(Class<X> type) {
        return new ObjectTemplate<>(type, x -> true);
    }

    @Override
    public boolean test(Tuple t) {
        return matches(t);
    }

}
