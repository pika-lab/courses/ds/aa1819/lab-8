package it.unibo.sd1819.lab6.ts.core;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface PredicativeTupleSpace<T extends Tuple, TT extends Template> extends TupleSpace<T, TT> {
    CompletableFuture<Optional<T>> tryTake(TT template);

    CompletableFuture<Optional<T>> tryRead(TT template);
}
