package it.unibo.sd1819.lab6.ts.objects;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.IntStream;

public abstract class TupleStream<T> {
    private final ObjectSpace<T> objectSpace;
    private final Function<ObjectTemplate<? extends T>, CompletableFuture<ObjectTuple<T>>> getter;
    private volatile boolean over = false;

    public TupleStream(ObjectSpace<T> objectSpace, boolean consume) {
        this.objectSpace = Objects.requireNonNull(objectSpace);
        this.getter = consume
                ? objectSpace::take
                : objectSpace::read;
        objectSpace.getExecutor().execute(() -> run(0));
    }

    public abstract ObjectTemplate<? extends T> onTemplate(int index);

    public abstract void onTuple(int index, ObjectTuple<T> tuple);

    private void run(int i) {
        getter.apply(onTemplate(i)).thenAcceptAsync(tuple -> {
            onTuple(i, tuple);
            if (!isOver()) {
                run(i + 1);
            }
        });
    }

    public boolean isOver() {
        return over;
    }

    public void stop() {
        over = true;
    }

    public static <X> TupleStream<X> ofReads(ObjectSpace<X> objectSpace, Function<Integer, ObjectTemplate<? extends X>> onTemplate, BiConsumer<Integer, ObjectTuple<X>> onTuple) {
        return new TupleStream<X>(objectSpace, false) {
            @Override
            public ObjectTemplate<? extends X> onTemplate(int index) {
                return onTemplate.apply(index);
            }

            @Override
            public void onTuple(int index, ObjectTuple<X> tuple) {
                onTuple.accept(index, tuple);
            }
        };
    }

    public static <X> TupleStream<X> ofTakes(ObjectSpace<X> objectSpace, Function<Integer, ObjectTemplate<? extends X>> onTemplate, BiConsumer<Integer, ObjectTuple<X>> onTuple) {
        return new TupleStream<X>(objectSpace, true) {
            @Override
            public ObjectTemplate<? extends X> onTemplate(int index) {
                return onTemplate.apply(index);
            }

            @Override
            public void onTuple(int index, ObjectTuple<X> tuple) {
                onTuple.accept(index, tuple);
            }
        };
    }

    public static void main(String... args) {
        class Msg {
            int index;
            String payload;

            public Msg(int index, String payload) {
                this.index = index;
                this.payload = payload;
            }

            @Override
            public String toString() {
                return "Msg{" +
                        "index=" + index +
                        ", payload='" + payload + '\'' +
                        '}';
            }
        }

        ObjectSpace<Msg> msgs = new ObjectSpace<>();

        IntStream.range(0, 10).forEach(i -> {
            msgs.write(ObjectTuple.of(
                    new Msg(i, UUID.randomUUID().toString())
            ));
        });

        // If Eclipse say this code doesn't compile, double-check with `./gradlew build`: it actually should according to javac
        TupleStream.ofReads(msgs,
                i -> ObjectTemplate.anyOfType(Msg.class).where(msg -> msg.index == i),
                (i, t) -> {
                    System.out.println(t);
                }
        );
    }
}
