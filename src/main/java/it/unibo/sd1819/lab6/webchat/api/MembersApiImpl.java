package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.BadContentError;
import it.unibo.sd1819.lab6.webchat.exceptions.ConflictError;
import it.unibo.sd1819.lab6.webchat.exceptions.ForbiddenError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotFoundError;
import it.unibo.sd1819.lab6.webchat.presentation.ChatRoom;
import it.unibo.sd1819.lab6.webchat.presentation.ChatRoom.AccessLevel;
import it.unibo.sd1819.lab6.webchat.presentation.Link;
import it.unibo.sd1819.lab6.webchat.presentation.ListOfUsers;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

public class MembersApiImpl extends AbstractApi implements MembersApi {

    MembersApiImpl(RoutingContext routingContext) {
        super(routingContext);
    }

    private ObjectSpace<ChatRoom> rooms = ObjectSpaces.chatRooms();

    @Override
    public void createChatRoomMember(String chatRoomName, User member, Handler<AsyncResult<Link>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        final User authenticatedUser = getAuthenticatedUser().get();

        rooms.tryRead(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))
        ).thenAcceptAsync(roomTuple -> {
            if (roomTuple.isPresent()) {
                final ChatRoom room = roomTuple.get().get();
                if (isOwnerOrAdmin(room)) {
                	//check if member exist
                	ObjectSpaces.users().tryRead(
                	        ObjectTemplate.anyOfType(User.class).where(u -> u.sameUserOf(member))
                    ).thenAcceptAsync(userTuple -> {
                		if(userTuple.isPresent()) {
                			addMember(room, userTuple.get().get(), handler);
                		} else {
                			handler.handle(Future.failedFuture(new BadContentError()));
                		}
                	});
                } else if (room.getAccessLevel() != AccessLevel.CLOSED) {
                    addMember(room, authenticatedUser, handler);
                } else {
                    handler.handle(Future.failedFuture(new ForbiddenError()));
                }
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    private void addMember(ChatRoom room, User member, Handler<AsyncResult<Link>> handler) {
        ObjectSpaces.chatRoomMembers(room.getName()).tryRead(
                ObjectTemplate.anyOfType(User.class).where(u -> u.sameUserOf(member))
        ).thenAcceptAsync(user -> {
            if (user.isPresent()) {
                handler.handle(Future.failedFuture(new ConflictError()));
            } else {
                ObjectSpaces.chatRoomMembers(room.getName()).write(
                        ObjectTuple.of(member)
                ).thenRunAsync(() -> {
                    handler.handle(Future.succeededFuture(room.getLink()));
                });
            }
        });
    }

    @Override
    public void deleteChatRoomMember(String chatRoomName, String memberId, Handler<AsyncResult<Void>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        final User authenticatedUser = getAuthenticatedUser().get();

        rooms.tryRead(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))
        ).thenAcceptAsync(roomTuple -> {
            if (roomTuple.isPresent()) {
                final ChatRoom room = roomTuple.get().get();

                ObjectSpaces.chatRoomMembers(room.getName()).tryRead(
                        ObjectTemplate.anyOfType(User.class).where(u -> u.isIdentifiedBy(memberId))
                ).thenAcceptAsync(userTuple -> {
                    if (userTuple.isPresent()) {
                        final User user = userTuple.get().get();
                        if (isOwnerOrAdmin(room) || user.sameUserOf(authenticatedUser)) {
                            ObjectSpaces.chatRoomMembers(room.getName()).take(
                                    ObjectTemplate.anyOfType(User.class).where(u -> u.sameUserOf(user))
                            ).thenRunAsync(() -> {
                                handler.handle(Future.succeededFuture());
                            });
                        } else {
                            handler.handle(Future.failedFuture(new ForbiddenError()));
                        }
                    } else {
                        handler.handle(Future.failedFuture(new NotFoundError()));
                    }
                });
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    @Override
    public void readChatRoomMembers(String chatRoomName, Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfUsers>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        final User authenticatedUser = getAuthenticatedUser().get();

        rooms.tryRead(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))
        ).thenAcceptAsync(roomTuple -> {
            if (roomTuple.isPresent()) {
                final ChatRoom room = roomTuple.get().get();
                if(isOwnerOrAdmin(room)) {
                    handleReadRoomMembers(room, skip, limit, filter, handler);
                } else {
                    ObjectSpaces.chatRoomMembers(room.getName()).tryRead(
                            ObjectTemplate.anyOfType(User.class).where(u -> u.sameUserOf(authenticatedUser))
                    ).thenAcceptAsync(member -> {
                        if (member.isPresent()) {
                            handleReadRoomMembers(room, skip, limit, filter, handler);
                        } else {
                            handler.handle(Future.failedFuture(new ForbiddenError()));
                        }
                    });
                }
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    private void handleReadRoomMembers(ChatRoom room, Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfUsers>> handler) {
        ObjectSpaces.chatRoomMembers(room.getName()).readAll(
                ObjectTemplate.anyOfType(User.class)
        ).thenAcceptAsync(members -> {
            final ListOfUsers result = new ListOfUsers(
                members.stream()
                        .map(ObjectTuple::get)
                        .filter(it -> it.getUsername().contains(filter))
                        .skip(skip)
                        .limit(limit)
            );
            handler.handle(Future.succeededFuture(result));
        });
    }

    private boolean isOwnerOrAdmin(final ChatRoom chatRoom) {
        return chatRoom.getOwner().sameUserOf(getAuthenticatedUser().get()) || isAuthenticatedUserAtLeast(User.Role.ADMIN);
    }
}
