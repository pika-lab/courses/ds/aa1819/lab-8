package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.ConflictError;
import it.unibo.sd1819.lab6.webchat.exceptions.ForbiddenError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotFoundError;
import it.unibo.sd1819.lab6.webchat.presentation.*;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RoomsApiImpl extends AbstractApi implements RoomsApi {

    RoomsApiImpl(RoutingContext routingContext) {
        super(routingContext);
    }

    private ObjectSpace<ChatRoom> rooms = ObjectSpaces.chatRooms();

    @Override
    public void createChatRoom(ChatRoom chatRoom, Handler<AsyncResult<Link>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        if (!(isAuthenticatedUserAtLeast(User.Role.ADMIN) && chatRoom.getOwner() != null)) {
            chatRoom.setOwner(getAuthenticatedUser().get());
        }

        rooms.tryRead(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.sameChatRoomOf(chatRoom))
        ).thenAcceptAsync(other -> {
            if (!other.isPresent()) {
                final ChatRoom newRoom = new ChatRoom(chatRoom).setMembers(null);

                rooms.write(ObjectTuple.of(newRoom)).thenRunAsync(() -> {
                	//add members, if any
                	if(chatRoom.getMembers() != null && !chatRoom.getMembers().isEmpty()) {
                    	ObjectSpaces.chatRoomMembers(chatRoom.getName()).writeAll(
                            chatRoom.getMembers()
                                    .stream()
                                    .map(ObjectTuple::of)
                                    .collect(Collectors.toList())
                        ).thenRunAsync(() -> handler.handle(Future.succeededFuture(chatRoom.getLink())));
                    } else {
                        handler.handle(Future.succeededFuture(chatRoom.getLink()));
                    }
                });
            } else {
                handler.handle(Future.failedFuture(new ConflictError()));
            }
        });
    }

    @Override
    public void deleteChatRoom(String chatRoomName, Handler<AsyncResult<Void>> handler) {
        ensureAuthenticatedUserAtLeast(User.Role.USER);

        rooms.tryTake(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))
        ).thenAcceptAsync(room -> {
            if (room.isPresent()) {
                final User owner = room.get().get().getOwner();
                if (owner.sameUserOf(getAuthenticatedUser().get()) || isAuthenticatedUserAtLeast(User.Role.ADMIN)) {
                    CompletableFuture.allOf(
                            ObjectSpaces.chatRoomMembers(chatRoomName).takeAll(ObjectTemplate.anyOfType(User.class)),
                            ObjectSpaces.chatRoomMessages(chatRoomName).takeAll(ObjectTemplate.anyOfType(ChatMessage.class))
                    ).thenRunAsync(() -> handler.handle(Future.succeededFuture()));
                } else {
                    handler.handle(Future.failedFuture(new ForbiddenError()));
                }
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    @Override
    public void readAllChatRooms(Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfChatRooms>> handler) {
        rooms.readAll(ObjectTemplate.anyOfType(ChatRoom.class)).thenAcceptAsync(rooms -> {
            final Stream<ChatRoom> chatRooms = rooms.stream()
                    .map(ObjectTuple::get)
                    .skip(skip)
                    .limit(limit)
                    .filter(r -> r.getName().contains(filter));
            handler.handle(Future.succeededFuture(new ListOfChatRooms(chatRooms)));
        });
    }

    @Override
    public void readChatRoom(String chatRoomName, Integer limitMessages, Integer limitMembers, Handler<AsyncResult<ChatRoom>> handler) {
        rooms.tryRead(ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))).thenAcceptAsync(roomTuple -> {
            if (roomTuple.isPresent()) {
                final ChatRoom room = new ChatRoom(roomTuple.get().get());
                final Future<Void> authorization = Future.future();

                authorizeUserForReadChatRoom(room, authorization.completer()); // make sure that the user is authorized to access the chat-room

                authorization.setHandler(x -> {
                    if (x.failed()) {
                        handler.handle(Future.failedFuture(x.cause()));
                    } else {
                        readChatRoomMembersAndMessages(room, limitMessages, limitMembers, handler);
                    }
                });
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    private void authorizeUserForReadChatRoom(ChatRoom room, Handler<AsyncResult<Void>> handler) {
        if (isAuthenticatedUserAtLeast(User.Role.ADMIN)) { // admins can read any chat room
            handler.handle(Future.succeededFuture());
        } else {
            if (isAuthenticatedUserAtLeast(User.Role.USER)) {
                final User user = getAuthenticatedUser().get();

                if(room.getOwner().sameUserOf(user)) {
                    handler.handle(Future.succeededFuture());
                } else {
                    ObjectSpaces.chatRoomMembers(room.getName()).tryRead(
                            ObjectTemplate.anyOfType(User.class).where(u -> u.sameUserOf(getAuthenticatedUser().get()))
                    ).thenAcceptAsync(member -> {
                        if(!member.isPresent()) { // user doesn't own the room,nor is it a member
                            handler.handle(Future.failedFuture(new ForbiddenError()));
                        }
                    });
                }
            } else if (room.getAccessLevel().compareTo(ChatRoom.AccessLevel.PUBLIC) > 0) { // room is not public
                handler.handle(Future.failedFuture(new ForbiddenError()));
            } else {
                handler.handle(Future.succeededFuture());
            }
        }
    }

    private void readChatRoomMembersAndMessages(ChatRoom room, Integer limitMessages, Integer limitMembers, Handler<AsyncResult<ChatRoom>> handler) {
        ObjectSpaces.chatRoomMembers(room.getName()).readAll(ObjectTemplate.anyOfType(User.class)).thenAcceptAsync(members -> {
            room.setMembersFromStream(members.stream().map(ObjectTuple::get).limit(limitMembers));
            room.setMembersCount(members.size());

            ObjectSpaces.chatRoomMessages(room.getName()).readAll(ObjectTemplate.anyOfType(ChatMessage.class)).thenAcceptAsync(messages -> {
                room.setMessagesFromStream(messages.stream().map(ObjectTuple::get).sorted().limit(limitMessages));
                room.setMessagesCount(messages.size());

                handler.handle(Future.succeededFuture(room));
            });
        });
    }
}
