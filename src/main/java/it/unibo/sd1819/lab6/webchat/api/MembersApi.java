package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.webchat.presentation.Link;
import it.unibo.sd1819.lab6.webchat.presentation.ListOfUsers;
import it.unibo.sd1819.lab6.webchat.presentation.User;

public interface MembersApi extends Api {

    void createChatRoomMember(String chatRoomName, User member, Handler<AsyncResult<Link>> handler);
    

    void deleteChatRoomMember(String chatRoomName, String memberId, Handler<AsyncResult<Void>> handler);
    

    void readChatRoomMembers(String chatRoomName, Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfUsers>> handler);

    static MembersApi get(RoutingContext context) {
        return new MembersApiImpl(context);
    }
}
