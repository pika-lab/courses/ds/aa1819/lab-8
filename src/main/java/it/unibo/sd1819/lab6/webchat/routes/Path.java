package it.unibo.sd1819.lab6.webchat.routes;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Stream;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.ErrorHandler;
import io.vertx.ext.web.handler.LoggerHandler;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.BadContentError;
import it.unibo.sd1819.lab6.webchat.exceptions.HttpError;
import it.unibo.sd1819.lab6.webchat.exceptions.InternalServerError;
import it.unibo.sd1819.lab6.webchat.presentation.Representation;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

public abstract class Path {

    protected Router router;
    private String path;
    private String parentPath = "";

	public Path(String subPath) {
		this.path = Objects.requireNonNull(subPath);

	}

	protected abstract void setupRoutes();
	
	protected Route addRoute(HttpMethod method, String path, Handler<RoutingContext> handler) {
		final Route route = method != null
                ? router.route(method, getPath() + path)
                : router.route(getPath() + path);
		return route
	        .handler(LoggerHandler.create())
	    	.handler(BodyHandler.create())
	    	.handler(handler)
	    	.handler(ErrorHandler.create());
    }

    protected Route addWebSocketRoute(HttpMethod method, String path, Handler<ServerWebSocket> handler) {
        final Route route = method != null
                ? router.route(method, getPath() + path)
                : router.route(getPath() + path);
        return route
                .handler(LoggerHandler.create())
                .handler(rc -> handler.handle(rc.request().upgrade()))
                .handler(ErrorHandler.create());
    }

    protected Route addWebSocketRoute(String path, Handler<ServerWebSocket> handler) {
        return addWebSocketRoute(null, path, handler);
    }

	protected Route addRoute(HttpMethod method, Handler<RoutingContext> handler) {
		return addRoute(method, "", handler);
	}

    protected Route addRoute(String path, Handler<RoutingContext> handler) {
        return addRoute(null, path, handler);
    }

    protected Route addRoute(Handler<RoutingContext> handler) {
        return addRoute(null, "", handler);
    }

	protected String getPath() {
		return parentPath + path;
	}

	protected String getSubPath(String subResource) {
		return getPath() + "/" + subResource;
	}

    protected  <X extends Representation> Handler<AsyncResult<X>> responseHandler(RoutingContext routingContext) {
	    return responseHandler(routingContext, Function.identity());
    }

	protected  <X extends Representation> Handler<AsyncResult<X>> responseHandler(RoutingContext routingContext, Function<X, X> cleaner) {
		return x -> {
			if (x.failed() && x.cause() instanceof HttpError) {
				final HttpError exception = (HttpError) x.cause();
				routingContext.response()
						.setStatusCode(exception.getStatusCode())
						.end(exception.getMessage());
			} else if (x.failed()) {
				routingContext.response()
						.setStatusCode(500)
						.end("Internal Server Error");

			} else {
			    try {
                    final X cleanResult = cleaner.apply(x.result());
                    String result = cleanResult.toMIMETypeString(routingContext.getAcceptableContentType());
                    routingContext.response()
                            .putHeader(HttpHeaders.CONTENT_TYPE, routingContext.getAcceptableContentType())
                            .setStatusCode(200)
                            .end(result);
                } catch (NullPointerException | IllegalArgumentException e) {
                    routingContext.response()
                            .setStatusCode(500)
                            .end("Internal Server Error");
                }
			}
		};
	}

	protected Handler<AsyncResult<Void>> responseHandlerWithNoContent(RoutingContext routingContext) {
		return x -> {
			if (x.failed() && x.cause() instanceof HttpError) {
				final HttpError exception = (HttpError) x.cause();
				routingContext.response()
						.setStatusCode(exception.getStatusCode())
						.end(exception.getMessage());
			} else if (x.failed()) {
				routingContext.response()
						.setStatusCode(500)
						.end("Internal Server Error");

			} else {
				routingContext.response()
						.setStatusCode(204)
						.end();
			}
		};
	}

	public void attach(Router router) {
        this.router = Objects.requireNonNull(router);
        setupRoutes();
    }

    public Path append(Path route) {
        route.parentPath = parentPath + path;
        route.attach(router);
        return route;
    }

    public Path append(String subPath, Path route) {
        route.parentPath = parentPath + path + subPath;
        route.attach(router);
        return route;
    }

    protected static void requireAllAreNull(Object x, Object... xs) {
        if (Stream.concat(Stream.of(x), Stream.of(xs)).anyMatch(Objects::nonNull)) {
            throw new BadContentError();
        }
    }

    protected static void requireAllAreNullOrEmpty(Collection<?> x, Collection<?>... xs) {
        if (Stream.concat(Stream.of(x), Stream.of(xs))
                .filter(Objects::nonNull).anyMatch(it -> it.size() > 0)) {
            throw new BadContentError();
        }
    }

    protected static void requireNoneIsNull(Object x, Object... xs) {
        if (Stream.concat(Stream.of(x), Stream.of(xs)).anyMatch(Objects::isNull)) {
            throw new BadContentError();
        }
    }

    protected static void requireSomeIsNonNull(Object x, Object... xs) {
        if (!Stream.concat(Stream.of(x), Stream.of(xs)).anyMatch(Objects::nonNull)) {
            throw new BadContentError();
        }
    }

    protected Optional<User> enrichUser(User user) {
        try {
            final Optional<ObjectTuple<User>> u = ObjectSpaces.users().tryRead(ObjectTemplate.anyOfType(User.class).where(it -> it.sameUserOf(user))).get();

            return u.map(ObjectTuple::get).map(User::new);
        } catch (InterruptedException | ExecutionException e) {
            throw new InternalServerError(e);
        }
    }

	protected Optional<User> findUser(String identifier) {
		try {
			final Optional<ObjectTuple<User>> u = ObjectSpaces.users().tryRead(ObjectTemplate.anyOfType(User.class).where(it -> it.isIdentifiedBy(identifier))).get();

			return u.map(ObjectTuple::get).map(User::new);
		} catch (InterruptedException | ExecutionException e) {
			throw new InternalServerError(e);
		}
	}


}
