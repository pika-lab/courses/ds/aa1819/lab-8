package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.ts.objects.TupleStream;
import it.unibo.sd1819.lab6.webchat.exceptions.ForbiddenError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotFoundError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotImplementedError;
import it.unibo.sd1819.lab6.webchat.presentation.ChatMessage;
import it.unibo.sd1819.lab6.webchat.presentation.ChatRoom;
import it.unibo.sd1819.lab6.webchat.presentation.ChatRoom.AccessLevel;
import it.unibo.sd1819.lab6.webchat.presentation.ListOfMessages;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

public class MessagesApiImpl extends AbstractApi implements MessagesApi {

    MessagesApiImpl(RoutingContext routingContext) {
        super(routingContext);
    }

    private ObjectSpace<ChatRoom> rooms = ObjectSpaces.chatRooms();

    @Override
    public void createChatRoomMessage(String chatRoomName, ChatMessage newMessage, Handler<AsyncResult<ChatMessage>> handler) {
        rooms.tryRead(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))
        ).thenAcceptAsync(roomTuple -> {
            if (roomTuple.isPresent()) {
                final ChatRoom room = roomTuple.get().get();
                final Future<Void> authorization = Future.future();
                authorizeUserForReadChatRoom(room, authorization.completer());

                authorization.setHandler(x -> {
                    if (x.failed()) {
                        handler.handle(Future.failedFuture(x.cause()));
                    } else {
                        ObjectSpaces.chatRoomMessages(room.getName()).readAll(
                                ObjectTemplate.anyOfType(ChatMessage.class)
                        ).thenAcceptAsync(messages -> {
                            ChatMessage message = new ChatMessage(newMessage)
                                .setChatRoom(room.getLink())
                                .setIndex(messages.size())
                                .setTimestampToNow()
                                .setSender(getAuthenticatedUser().orElse(null));

                            ObjectSpaces.chatRoomMessages(room.getName()).write(ObjectTuple.of(message))
                                    .thenRunAsync(() -> handler.handle(Future.succeededFuture(message)));
                            });
                    }
                });


            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    @Override
    public void readChatRoomMessages(String chatRoomName, Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfMessages>> handler) {
        rooms.tryRead(
                ObjectTemplate.anyOfType(ChatRoom.class).where(it -> it.isIdentifiedBy(chatRoomName))
        ).thenAcceptAsync(roomTuple -> {
            if (roomTuple.isPresent()) {
                final ChatRoom room = roomTuple.get().get();
                final Future<Void> authorization = Future.future();
                authorizeUserForReadChatRoom(room, authorization.completer());

                authorization.setHandler(x -> {
                    if (x.failed()) {
                        handler.handle(Future.failedFuture(x.cause()));
                    } else {
                        ObjectSpaces.chatRoomMessages(room.getName()).readAll(
                                ObjectTemplate.anyOfType(ChatMessage.class)
                        ).thenAcceptAsync(messages -> {
                            final ListOfMessages result = new ListOfMessages(
                                messages.stream()
                                    .map(ObjectTuple::get)
                                    .sorted()
                                    .filter(m -> m.getContent().contains(filter))
                                    .skip(skip)
                                    .limit(limit)
                            );

                            handler.handle(Future.succeededFuture(result));
                        });
                    }
                });
            } else {
                handler.handle(Future.failedFuture(new NotFoundError()));
            }
        });
    }

    private void authorizeUserForReadChatRoom(ChatRoom room, Handler<AsyncResult<Void>> handler) {
        if (isAuthenticatedUserAtLeast(User.Role.ADMIN)) { // admins can read any chat room
            handler.handle(Future.succeededFuture());
        } else {
            if (isAuthenticatedUserAtLeast(User.Role.USER)) {

                final User user = getAuthenticatedUser().get();

                if (room.getOwner().sameUserOf(user)) {
                    handler.handle(Future.succeededFuture());
                } else {
                    ObjectSpaces.chatRoomMembers(room.getName()).tryRead(
                            ObjectTemplate.anyOfType(User.class).where(u -> u.sameUserOf(user))
                    ).thenAcceptAsync(member -> {
                        if(!member.isPresent() && room.getAccessLevel() != AccessLevel.PUBLIC) {
                            handler.handle(Future.failedFuture(new ForbiddenError()));
                        } else {
                            handler.handle(Future.succeededFuture());
                        }
                    });
                }
            } else if (room.getAccessLevel().compareTo(ChatRoom.AccessLevel.PUBLIC) > 0) { // room is not public
                handler.handle(Future.failedFuture(new ForbiddenError()));
            } else {
                handler.handle(Future.succeededFuture());
            }
        }
    }

    @Override
    public void openMessageStream(String chatRoomName, Integer skip, Handler<AsyncResult<Void>> handler) {

        // TODO perform all necessary checks, just like in the `readChatRoomMessages` case

        // TODO in case the checks succeed, upgrade the request

        // TODO Then handle the message stream :)

        handler.handle(Future.failedFuture(new NotImplementedError()));
    }

    private void handleMessageStream(String chatRoomName, Integer skip, String targetType, ServerWebSocket ws) {
        final ObjectSpace<ChatMessage> messages = ObjectSpaces.chatRoomMessages(chatRoomName);

        final TupleStream<ChatMessage> stream = TupleStream.ofReads(
                messages,
                i -> null, // TODO select the correct message
                (i, t) -> {} // TODO send it to the client
        );

        // TODO handle the situation where the WS is closed by the client
    }

}
