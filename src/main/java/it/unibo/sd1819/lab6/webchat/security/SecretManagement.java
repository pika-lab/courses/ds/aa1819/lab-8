package it.unibo.sd1819.lab6.webchat.security;

import java.util.Objects;
import java.util.Optional;

public class SecretManagement {
    private static final String DEFAULT_SECRET = "this is NOT a correct way of storing secrets";

    private static String secret = DEFAULT_SECRET;

    public static String getSecret() {
        return secret;
    }

    public static void setSecret(String secret) {
        SecretManagement.secret = Objects.requireNonNull(secret);
    }

    public static void setSecret(Optional<String> secret) {
        setSecret(secret.orElse(DEFAULT_SECRET));
    }
}
