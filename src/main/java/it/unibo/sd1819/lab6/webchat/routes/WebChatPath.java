package it.unibo.sd1819.lab6.webchat.routes;

public class WebChatPath extends Path {

	public WebChatPath(String version) {
		super("/web-chat/v" + version);
	}

    @Override
    protected void setupRoutes() {
        append(new UsersPath());
        append(new RoomsPath());
        append(new AuthPath());
    }
}
