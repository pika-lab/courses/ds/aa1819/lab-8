package it.unibo.sd1819.lab6.webchat.storage;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import it.unibo.sd1819.lab6.ts.AbstractTupleSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectSpace;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.Service;
import it.unibo.sd1819.lab6.webchat.presentation.ChatMessage;
import it.unibo.sd1819.lab6.webchat.presentation.ChatRoom;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.security.SecurityUtils;

import java.util.*;

public class ObjectSpaces {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectSpaces.class);
    private static Set<ObjectSpace<?>> localSpaces;

    public static <T> ObjectSpace<T> get(String name) {
        final Optional<ObjectSpace<?>> space = findByName(name);
        if (space.isPresent()) {
            return (ObjectSpace<T>) space.get();
        } else {
            final ObjectSpace<T> newObjSpace = new ObjectSpace<>(name);
            localSpaces.add(newObjSpace);
            return newObjSpace;
        }
    }

    private static Optional<ObjectSpace<?>> findByName(String name) {
        return localSpaces.stream().filter(it -> it.getName().contains(name)).findFirst();
    }

    public static ObjectSpace<User> users() {
        return get(User.class.getName());
    }

    public static ObjectSpace<User> chatRoomMembers(String chatRoomName) {
        return get(User.class.getName() + "." + chatRoomName);
    }

    public static ObjectSpace<ChatMessage> chatRoomMessages(String chatRoomName) {
        return get(ChatMessage.class.getName() + "." + chatRoomName);
    }

    public static ObjectSpace<ChatRoom> chatRooms() {
        return get(ChatRoom.class.getName());
    }

    public static void reset() {
        localSpaces = new TreeSet<>(Comparator.comparing(AbstractTupleSpace::getName));

        final User admin = new User()
                .setId(UUID.randomUUID())
                .setFullName("Admin Nimda")
                .setUsername("admin")
                .setEmail("web-chat-admin@unibo.it")
                .setRole(User.Role.ADMIN)
                .setPassword(SecurityUtils.hashPassword("admin")) // TODO notice me!
                .setLinkUrl("/web-chat/v2/users/admin");

        final User user = new User()
                .setId(UUID.randomUUID())
                .setFullName("User Resu")
                .setUsername("user")
                .setEmail("user.resu@studio.unibo.it")
                .setRole(User.Role.USER)
                .setPassword(SecurityUtils.hashPassword("user")) // TODO notice me!
                .setLinkUrl("/web-chat/v2/users/user");

        final ChatRoom wellcome = new ChatRoom()
                .setAccessLevel(ChatRoom.AccessLevel.PUBLIC)
                .setName("wellcome")
                .setOwner(new User(admin))
                .setMembersCount(1)
                .setMessagesCount(1)
                .setLinkUrl("/web-chat/v2/rooms/wellcome");

        final ChatMessage message = new ChatMessage()
                .setIndex(0)
                .setContent("Hello everybody!")
                .setSender(new User(admin))
                .setChatRoom(wellcome.getLink())
                .setTimestampToNow();

        try {
            users().write(ObjectTuple.of(admin)).get();
            LOGGER.info("Admin user: " + admin);
            users().write(ObjectTuple.of(user)).get();
            LOGGER.info("Normal user: " + user);

            chatRooms().write(ObjectTuple.of(wellcome)).get();
            LOGGER.info("Wellcome romm: " + user);

            chatRoomMessages(wellcome.getName()).write(ObjectTuple.of(message)).get();
            chatRoomMembers(wellcome.getName()).write(ObjectTuple.of(new User(user))).get();
        } catch (Exception e) {
            LOGGER.fatal("Could not initialize the admin user", e);
        }
    }
}
