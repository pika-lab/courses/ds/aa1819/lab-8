package it.unibo.sd1819.lab6.webchat.routes;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.webchat.api.AuthApi;
import it.unibo.sd1819.lab6.webchat.api.UsersApi;
import it.unibo.sd1819.lab6.webchat.exceptions.BadContentError;
import it.unibo.sd1819.lab6.webchat.exceptions.HttpError;
import it.unibo.sd1819.lab6.webchat.presentation.Link;
import it.unibo.sd1819.lab6.webchat.presentation.Representation;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.security.JsonWebToken;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import static it.unibo.sd1819.lab6.webchat.presentation.MIMETypes.*;

// TODO notice me!
public class AuthPath extends Path {


    public AuthPath() {
        super("/auth");
    }

	@Override
	protected void setupRoutes() {
        addRoute(HttpMethod.POST, "/token", this::post)
                .consumes(APPLICATION_JSON)
                .consumes(APPLICATION_XML)
                .consumes(APPLICATION_YAML)
                .produces(APPLICATION_JWT);
	}

	private void post(RoutingContext routingContext) {
		final AuthApi api = AuthApi.get(routingContext);
        final Future<JsonWebToken> result = Future.future();
        result.setHandler(jwtResponseHandler(routingContext));

		try {
			final User user = User.parse(routingContext.parsedHeaders().contentType().value(), routingContext.getBodyAsString());
            final Optional<Integer> duration = Optional.ofNullable(routingContext.queryParams().get("duration")).map(Integer::parseInt);

            validateCredentialsForPost(user);

			api.createToken(user, duration.orElse(null), result.completer());
		} catch(HttpError e) {
            result.fail(e);
        } catch (IOException | IllegalArgumentException e) {
			result.fail(new BadContentError(e));
		}
	}

    private void validateCredentialsForPost(User user) {
        requireNoneIsNull(user.getPassword());
        requireSomeIsNonNull(user.getId(), user.getEmail(), user.getUsername());

    }


    protected  <X extends JsonWebToken> Handler<AsyncResult<X>> jwtResponseHandler(RoutingContext routingContext) {
        return x -> {
            if (x.failed() && x.cause() instanceof HttpError) {
                final HttpError exception = (HttpError) x.cause();
                routingContext.response()
                        .setStatusCode(exception.getStatusCode())
                        .end(exception.getMessage());
            } else if (x.failed()) {
                routingContext.response()
                        .setStatusCode(500)
                        .end("Internal Server Error");
            } else {
                try {
                    final X cleanResult = x.result();
                    final String result = cleanResult.toBase64Signed();
                    routingContext.response()
                            .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JWT)
                            .setStatusCode(200)
                            .end(result);
                } catch (NullPointerException | IllegalArgumentException e) {
                    routingContext.response()
                            .setStatusCode(500)
                            .end("Internal Server Error");
                }
            }
        };
    }

}
