package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.security.JsonWebToken;

public interface AuthApi extends Api {

    void createToken(User credentials, Integer duration, Handler<AsyncResult<JsonWebToken>> handler);

    static AuthApi get(RoutingContext context) {
        return new AuthApiImpl(context);
    }
}