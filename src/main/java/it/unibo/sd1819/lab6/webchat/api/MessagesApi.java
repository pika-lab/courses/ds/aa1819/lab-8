package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.webchat.presentation.ChatMessage;
import it.unibo.sd1819.lab6.webchat.presentation.Link;
import it.unibo.sd1819.lab6.webchat.presentation.ListOfMessages;

import java.util.List;
import java.util.function.Supplier;

public interface MessagesApi extends Api {

    void createChatRoomMessage(String chatRoomName, ChatMessage newMessage, Handler<AsyncResult<ChatMessage>> handler);
    

    void readChatRoomMessages(String chatRoomName, Integer skip, Integer limit, String filter, Handler<AsyncResult<ListOfMessages>> handler);

    void openMessageStream(String chatRoomName, Integer skip, Handler<AsyncResult<Void>> handler);


    static MessagesApi get(RoutingContext context) {
        return new MessagesApiImpl(context);
    }
}
