package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.webchat.exceptions.NotFoundError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotImplementedError;
import it.unibo.sd1819.lab6.webchat.exceptions.UnauthorizedError;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.security.JsonWebToken;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

public class AuthApiImpl extends AbstractApi implements AuthApi {

    public AuthApiImpl(RoutingContext routingContext) {
        super(routingContext);
    }

    @Override
    public void createToken(User credentials, Integer duration, Handler<AsyncResult<JsonWebToken>> handler) {
        // TODO handle token generation / error management
        handler.handle(Future.failedFuture(new NotImplementedError()));
    }

    private JsonWebToken generateToken(User user, Optional<Integer> duration) {
        final OffsetDateTime now = OffsetDateTime.now();

        final JsonWebToken jwt = new JsonWebToken();
        jwt.getHeader()
                .setAlg(JsonWebToken.Header.ALG_SIMPLE_HMAC)
                .setTyp(JsonWebToken.Header.TYP_JWT);

        // TODO set the JWT payload, including, at least, the following claims:
        // TODO - jid
        // TODO - iat
        // TODO - exp, in case duration is preset
        // TODO - x-user

        // TODO set the JWT signature

        return jwt;
    }
}
