package it.unibo.sd1819.lab6.webchat.presentation;

import io.vertx.ext.web.MIMEHeader;

import java.util.Objects;

public final class MIMETypes {

    public static String APPLICATION_JSON = "application/json";

    public static String APPLICATION_YAML = "application/yaml";

    public static String APPLICATION_XML = "application/xml";

    public static String APPLICATION_JWT = "application/jwt";

    public static String TEXT_HTML = "text/html";

    public static String TEXT_PLAIN = "text/plain";

    public static String ANY = "*/*";

    public static String APPLICATION_ANY = "application/*";
}
