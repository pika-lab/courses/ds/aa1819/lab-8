package it.unibo.sd1819.lab6.webchat.api;

import io.vertx.ext.web.RoutingContext;
import it.unibo.sd1819.lab6.ts.objects.ObjectTemplate;
import it.unibo.sd1819.lab6.ts.objects.ObjectTuple;
import it.unibo.sd1819.lab6.webchat.exceptions.ForbiddenError;
import it.unibo.sd1819.lab6.webchat.exceptions.NotImplementedError;
import it.unibo.sd1819.lab6.webchat.exceptions.UnauthorizedError;
import it.unibo.sd1819.lab6.webchat.presentation.User;
import it.unibo.sd1819.lab6.webchat.security.JsonWebToken;
import it.unibo.sd1819.lab6.webchat.storage.ObjectSpaces;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

abstract class AbstractApi implements Api {

    private static final Pattern JWT_PATTERN = Pattern.compile("^(?:(?:[jJ][wW][tT])?\\s+)([^\\s]*)\\s*$");

    private final RoutingContext routingContext;

    AbstractApi(RoutingContext routingContext) {
        this.routingContext = Objects.requireNonNull(routingContext);
    }

    @Override
    public RoutingContext getRoutingContext() {
        return routingContext;
    }

    protected Optional<User> getAuthenticatedUser() {
        Optional<User> credentials = Optional.empty();

        try {
            if (getAuthorizationHeader() != null) {
                final Matcher m = JWT_PATTERN.matcher(getAuthorizationHeader());
                if (m.matches()) {
                    final String jwtString = m.group(1);
                    final JsonWebToken jwt = JsonWebToken.fromBase64(jwtString);

                    // TODO verify the JWT signature according to the algorithm declared within the JWT header

                    // TODO return 401 status code in case the signature is invalid

                    throw new NotImplementedError();
                } else {
                    throw new UnauthorizedError();
                }
            }
        } catch (IOException e) {
            throw new UnauthorizedError(e);
        }


        return credentials;
    }

    protected boolean isAuthenticatedUserAtLeast(User.Role role) {
        return getAuthenticatedUser().isPresent() && getAuthenticatedUser().get().getRole().compareTo(role) >= 0;
    }

    protected void ensureAuthenticatedUserAtLeast(User.Role role) {
        if (!getAuthenticatedUser().isPresent()) {
            throw new UnauthorizedError();
        }
        if (getAuthenticatedUser().get().getRole().compareTo(role) < 0) {
            throw new ForbiddenError();
        }
    }
}
